package com.indicadores.bonita.business;

import com.indicadores.bonita.adapter.RestAdapter;
import com.indicadores.bonita.dto.*;
import com.indicadores.bonita.mapper.*;
import com.indicadores.bonita.repository.*;
import com.indicadores.bonita.repository.domain.*;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

@Service
public class UsuarioBusiness {

    UsuarioRepository usuarioRepository;

    public UsuarioBusiness(){

    }

    public UsuarioBusiness(UsuarioRepository usuarioRepository){
        this.usuarioRepository = usuarioRepository;
    }

    public Respuesta registrarUsuarioBusiness(UsuarioDto usuarioDto){

        Respuesta respuesta = new Respuesta();

        //Validar que no exista
        System.out.println(usuarioDto.getEmail());
        List<Usuario> listUsuario = usuarioRepository.findByEmail(usuarioDto.getEmail());

        if (listUsuario.size()>0) {
            respuesta.setStatusCode(1);
            respuesta.setMessage("email ya existe");
            return respuesta;
        }

        //mapear a BD
        UsuarioMapper usuarioMapper = new UsuarioMapper();
        //guardar en BD
        usuarioRepository.save(usuarioMapper.fromUsuarioDtoToUsuario(usuarioDto));
        //responder
        respuesta.setStatusCode(0);
        respuesta.setMessage("Exitoso");
        return respuesta;
    }

    public DespachoRtaDto consultarDespachos (Integer idClient, DespachoRepository despachoRepository){

        DespachoMapper despachoMapper = new DespachoMapper();
        DespachoRtaDto despachoRtaDto = new DespachoRtaDto();
        Respuesta respuesta = new Respuesta();
        List<DespachoDto> listDespachoDto = new ArrayList<>();

        List<Despacho> listaDespachos = despachoRepository.findByIdclient(idClient);
        if (listaDespachos.size()<1){
            respuesta.setStatusCode(3);
            respuesta.setMessage("Cliente sin despachos asociados");
            despachoRtaDto.setRespuesta(respuesta);
            return despachoRtaDto;
        }

        for (Despacho despacho : listaDespachos) {
            listDespachoDto.add(despachoMapper.fromDespachoToDespachoDto(despacho));
        }

        respuesta.setStatusCode(0);
        respuesta.setMessage("Exitoso");
        despachoRtaDto.setRespuesta(respuesta);
        despachoRtaDto.setListDespachoDto(listDespachoDto);
        return despachoRtaDto;
    }

    public UsuarioRtaDto loginUsuario (LoginDto loginDto){

        UsuarioRtaDto usuarioRtaDto = new UsuarioRtaDto();
        Respuesta respuesta = new Respuesta();
        List<Usuario> listUsuario = usuarioRepository.findByEmail(loginDto.getEmail());
        UsuarioMapper usuarioMapper = new UsuarioMapper();

        if (listUsuario.size()<1){
            respuesta.setStatusCode(5);
            respuesta.setMessage("Usuario no existe");
            usuarioRtaDto.setRespuesta(respuesta);
            return usuarioRtaDto;
        }

        if (!loginDto.getPassword().equals(listUsuario.get(0).getPassword())){
            respuesta.setStatusCode(6);
            respuesta.setMessage("Usuario o password incorrecto");
            usuarioRtaDto.setRespuesta(respuesta);
            return usuarioRtaDto;
        }

        respuesta.setStatusCode(0);
        respuesta.setMessage("Exitoso");
        usuarioRtaDto.setRespuesta(respuesta);
        usuarioRtaDto.setUsuarioDto(usuarioMapper.fromUsuarioToUsuarioDto(listUsuario.get(0)));
        return usuarioRtaDto;

    }

    public RespuestaOrdenDto crearDespachoLocal(RequestCreateOrderKsDto requestCreateOrderKsDto, OrdenRepository ordenRepository, VendorConfigurationRepository vendorConfigurationRepository){
        RespuestaOrdenDto respuesta = new RespuestaOrdenDto();
        OrdenMapper ordenMapper = new OrdenMapper();
        RestAdapter restAdapter = new RestAdapter();
        EmailMapper emailMapper = new EmailMapper();
        Orden orden = new Orden();
        //llama al proveedor de mensajeria
        List<VendorConfiguration> listaProveedores = vendorConfigurationRepository.findByState("M");
        //Cotizacion en futuros
        CompletableFuture<VendorMsj>[] result = new CompletableFuture[listaProveedores.size()];
        int contResult = 0;
        //Llamado al servicio de mensjearia
        CompletableFuture<VendorMsj> jsonObjectProducto = restAdapter.adpRestGET(listaProveedores.get(0).getEndpointms());
        //crear elemento para almacenamiento en BD
        try {
            if (!jsonObjectProducto.get().getCarrierName().isEmpty()){
                orden = ordenRepository.save(ordenMapper.fronDtoToTbl(requestCreateOrderKsDto,jsonObjectProducto.get().getCarrierName()));
            }
        } catch (Exception e) {
            System.out.println(e.getStackTrace().toString());
        }

        //llamado al servicio de correo
        CompletableFuture<Object> jsonObject = restAdapter.adpRestPOST(emailMapper.createRequestEmail(orden),"http://apinotication-env-1.eba-2yrikvni.us-east-2.elasticbeanstalk.com/api/Notification/email");

        respuesta.setStatusCode(0);
        respuesta.setMessage("Exitoso");
        respuesta.setIdOrden(orden.getIdorden());
        respuesta.setNameProviderMnj(orden.getProveedormnj());
        return respuesta;
    }

    public RespuestaOrdenDto crearDespachoProveedor(RequestCreateOrderKsDto requestCreateOrderKsDto, OrdenRepository ordenRepository, VendorConfigurationRepository vendorConfigurationRepository){
        System.out.println("init");
        RespuestaOrdenDto respuesta = new RespuestaOrdenDto();
        OrdenMapper ordenMapper = new OrdenMapper();
        RestAdapter restAdapter = new RestAdapter();
        EmailMapper emailMapper = new EmailMapper();
        Orden orden = new Orden();
        //llama al proveedor de mensajeria
        List<VendorConfiguration> listaProveedores = vendorConfigurationRepository.findByName(requestCreateOrderKsDto.getProveedor());
        //Crear orden en el proveedor
        CompletableFuture<VendorMsj>[] result = new CompletableFuture[listaProveedores.size()];
        int contResult = 0;
        //Llamado al servicio de mensjearia
        CompletableFuture<Respuesta> jsonObjectRespuesta = restAdapter.cotizarRest(requestCreateOrderKsDto,listaProveedores.get(0).getEndpointms());
        //crear elemento para almacenamiento en BD
        try {
            System.out.println("jsonObjectRespuesta.get().getMessage()" + jsonObjectRespuesta.get().getMessage());

            if (!jsonObjectRespuesta.get().getMessage().isEmpty()){
                System.out.println("provver: "+requestCreateOrderKsDto.getProveedor());
                System.out.println("idOrden: "+ jsonObjectRespuesta.get().getMessage());
                orden = ordenRepository.save(ordenMapper.fronDtoToTblOrdenProveedor(requestCreateOrderKsDto,jsonObjectRespuesta.get().getMessage()));
            }
        } catch (Exception e) {
            System.out.println(e.getStackTrace().toString());
        }

        //llamado al servicio de correo
        CompletableFuture<Object> jsonObject = restAdapter.adpRestPOST(emailMapper.createRequestEmail(orden),"http://apinotication-env-1.eba-2yrikvni.us-east-2.elasticbeanstalk.com/api/Notification/email");

        respuesta.setStatusCode(0);
        respuesta.setMessage("Exitoso");
        respuesta.setIdOrden(orden.getIdorden());
        respuesta.setNameProviderMnj(orden.getProveedormnj());
        return respuesta;
    }

    public Respuesta actualizarOferta(ConfirmarDespachoDto confirmarDespachoDto, OfertaRepository ofertaRepository, DespachoRepository despachoRepository){
        Respuesta respuesta = new Respuesta();
        OfertaMapper ofertaMapper = new OfertaMapper();

        Oferta oferta = ofertaRepository.findById(confirmarDespachoDto.getIdOferta()).get();
        if (oferta.getState().isEmpty()){
            respuesta.setStatusCode(3);
            respuesta.setMessage("No existe oferta");
            return respuesta;
        }


        //actualizar el despacho a Seleccionado
        Despacho despacho = despachoRepository.findById(oferta.getIddespacho()).get();
        despacho.setState(confirmarDespachoDto.getEstado());
        despachoRepository.save(despacho);

        //Actualizar las otras ofertas a rechazadas
        List<Oferta> listaOferta = ofertaRepository.findByIddespachoOrderByValueAsc(oferta.getIddespacho());
        if (listaOferta.size()>1){
            for (Oferta oferta1 : listaOferta) {
                oferta1.setState("Rechazada");
                ofertaRepository.save(oferta1);
            }
        }
        //Actualizar el estado de la oferta seleccionada
        oferta.setState(confirmarDespachoDto.getEstado());
        ofertaRepository.save(oferta);
        respuesta.setStatusCode(0);
        respuesta.setMessage("Exitoso");
        return respuesta;
    }

    public void enviarNotificacion(DespachoRepository despachoRepository, OfertaRepository ofertaRepository, int idDespacho, UsuarioRepository usuarioRepository){

        //Se validan las ofertas para el despacho

        List<Oferta> listOferta = ofertaRepository.findByIddespachoOrderByValueAsc(idDespacho);
        String cuerpo = "Se completaron las ofertas para el despacho: "+ idDespacho;

        for (Oferta oferta : listOferta) {
            System.out.println(oferta.getIdproveedor().toString());

            String nameProveedor = usuarioRepository.findById(oferta.getIdproveedor()).get().getNames();

            cuerpo = cuerpo+"\nValor: "+oferta.getValue()+"; Proveedor: " + nameProveedor;
        }

        Despacho despacho = despachoRepository.findById(idDespacho).get();

        String destinatario = usuarioRepository.findById(despacho.getIdclient()).get().getEmail();

        System.out.println(cuerpo);
        // Esto es lo que va delante de @gmail.com en tu cuenta de correo. Es el remitente también.
        String remitente = "alejandror28@gmail.com";  //Para la dirección nomcuenta@gmail.com

        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", "Rppla1984");    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        //String destinatario = "pedro.rojas@avaldigitallabs.com";
        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipients(Message.RecipientType.TO, destinatario);   //Se podrían añadir varios de la misma manera
            message.setSubject("[Farmaceutica Javeriana][Ofertas disponibles despacho "+idDespacho+"]");
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, "Rppla1984");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (Exception me) {
            me.printStackTrace();   //Si se produce un error
        }

        despacho.setState("Completado");
        despachoRepository.save(despacho);

    }

    public List<ProductoDto> listarProducto(VendorConfigurationRepository vendorConfigurationRepository){

        //consultar los proveedores

        List<VendorConfiguration> listaProveedores = vendorConfigurationRepository.findByState("A");
        //Cotizacion en futuros

        System.out.println(listaProveedores.size());
        RestAdapter restAdapter = new RestAdapter();
        List<ProductoDto> listProductDto = new ArrayList<>();

        CompletableFuture<RespuestaProductos>[] result = new CompletableFuture[listaProveedores.size()];

        int contResult = 0;

        for (int i = 0; i < listaProveedores.size(); i++) {
            //Llamado al servicio
            CompletableFuture<RespuestaProductos> jsonObjectProducto = restAdapter.adptRestGET(listaProveedores.get(i).getEndpointms());
            //agregando una respuesta
            result[contResult] = jsonObjectProducto;
            contResult++;
        }

        //Siempre se cotiza en BDlocal.
        CompletableFuture.allOf(result).join();
        IntStream.range(0, result.length).forEach(idx -> {
            try {
                if (result[idx].get().getStatusCode().equals("0")){
                    listProductDto.addAll(result[idx].get().getProducts());
                }
            } catch (Exception e) {
                System.out.println(e.getStackTrace().toString());
            }
        });
        System.out.println("result.length:"+listProductDto.size());
        return listProductDto;
    }
}