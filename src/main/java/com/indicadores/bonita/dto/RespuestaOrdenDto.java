package com.indicadores.bonita.dto;

public class RespuestaOrdenDto {
        private int statusCode;
        private String message;
        private String nameProviderMnj;
        private String idOrden;

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    public String getNameProviderMnj() {
        return nameProviderMnj;
    }

    public void setNameProviderMnj(String nameProviderMnj) {
        this.nameProviderMnj = nameProviderMnj;
    }

    public String getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(String idOrden) {
        this.idOrden = idOrden;
    }
}
