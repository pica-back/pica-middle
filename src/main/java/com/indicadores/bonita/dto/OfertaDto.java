package com.indicadores.bonita.dto;

import javax.persistence.Column;
import java.util.Date;

public class OfertaDto {
    private Integer id;
    private Integer idccotizacion;
    private Integer iddespacho;
    private Integer idproveedor;
    private Date date;
    private Integer idservicio;
    private float value;
    private String state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdccotizacion() {
        return idccotizacion;
    }

    public void setIdccotizacion(Integer idccotizacion) {
        this.idccotizacion = idccotizacion;
    }

    public Integer getIddespacho() {
        return iddespacho;
    }

    public void setIddespacho(Integer iddespacho) {
        this.iddespacho = iddespacho;
    }

    public Integer getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(Integer idproveedor) {
        this.idproveedor = idproveedor;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Integer idservicio) {
        this.idservicio = idservicio;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
