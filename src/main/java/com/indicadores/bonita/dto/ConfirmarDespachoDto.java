package com.indicadores.bonita.dto;

public class ConfirmarDespachoDto {
    private int idOferta;
    private String estado;

    public int getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(int idOferta) {
        this.idOferta = idOferta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
