package com.indicadores.bonita.dto;

import java.util.List;

public class RespuestaProductos {
    private String statusCode;
    private List<ProductoDto> products;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<ProductoDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductoDto> products) {
        this.products = products;
    }
}
