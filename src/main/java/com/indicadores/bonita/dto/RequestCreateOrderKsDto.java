package com.indicadores.bonita.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

public class RequestCreateOrderKsDto {

    private Integer id;
    private String email;
    private String idorden;
    private Date feccreacion;
    private Date fecactualizacion;
    private String direccion;
    private Integer valor;
    private Integer cantidad;
    private String idproductprovider;
    private String nomproducto;
    private String proveedor;
    private String proveedormj;
    private String estado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdorden() {
        return idorden;
    }

    public void setIdorden(String idorden) {
        this.idorden = idorden;
    }

    public Date getFeccreacion() {
        return feccreacion;
    }

    public void setFeccreacion(Date feccreacion) {
        this.feccreacion = feccreacion;
    }

    public Date getFecactualizacion() {
        return fecactualizacion;
    }

    public void setFecactualizacion(Date fecactualizacion) {
        this.fecactualizacion = fecactualizacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdproductprovider() {
        return idproductprovider;
    }

    public void setIdproductprovider(String idproductprovider) {
        this.idproductprovider = idproductprovider;
    }

    public String getNomproducto() {
        return nomproducto;
    }

    public void setNomproducto(String nomproducto) {
        this.nomproducto = nomproducto;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getProveedormj() {
        return proveedormj;
    }

    public void setProveedormj(String proveedormj) {
        this.proveedormj = proveedormj;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
