package com.indicadores.bonita.dto;

public class EnviarNotificacionDto {
    private int idDespacho;

    public int getIdDespacho() {
        return idDespacho;
    }

    public void setIdDespacho(int idDespacho) {
        this.idDespacho = idDespacho;
    }
}
