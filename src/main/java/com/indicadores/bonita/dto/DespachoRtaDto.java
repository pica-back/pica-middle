package com.indicadores.bonita.dto;

import java.util.List;

public class DespachoRtaDto {
    private Respuesta respuesta;
    private List<DespachoDto> listDespachoDto;

    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    public List<DespachoDto> getListDespachoDto() {
        return listDespachoDto;
    }

    public void setListDespachoDto(List<DespachoDto> listDespachoDto) {
        this.listDespachoDto = listDespachoDto;
    }
}
