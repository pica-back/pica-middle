package com.indicadores.bonita.dto;

import java.util.List;

public class OfertaRtaDto {
    private Respuesta respuesta;
    private List<OfertaDto> listOfeta;

    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    public List<OfertaDto> getListOfeta() {
        return listOfeta;
    }

    public void setListOfeta(List<OfertaDto> listOfeta) {
        this.listOfeta = listOfeta;
    }
}
