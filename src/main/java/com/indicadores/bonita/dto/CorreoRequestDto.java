package com.indicadores.bonita.dto;

public class CorreoRequestDto {
    private String sender; //": "kallsonysnotifications@gmail.com",
    private String reciever; //": "camiloandrescortesfuquene@gmail.com",
    private String subject; //": "test desde Postman",
    private String content; //": "prueba desde postman"

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
