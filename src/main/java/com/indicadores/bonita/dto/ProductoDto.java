package com.indicadores.bonita.dto;

import java.io.Serializable;

public class ProductoDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String subCategoryId;

    private String productServiceDescription;

    private String subCategoryDescription;
    private float value;
    private int quantity;
    private double totalScore;
    private String iDProductProvider;
    private String nameProvider;

    public String getiDProductProvider() {
        return iDProductProvider;
    }

    public void setiDProductProvider(String iDProductProvider) {
        this.iDProductProvider = iDProductProvider;
    }

    public Long getId() {
        return id;
    }

    public String getNameProvider() {
        return nameProvider;
    }

    public void setNameProvider(String nameProvider) {
        this.nameProvider = nameProvider;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getProductServiceDescription() {
        return productServiceDescription;
    }

    public void setProductServiceDescription(String productServiceDescription) {
        this.productServiceDescription = productServiceDescription;
    }

    public String getSubCategoryDescription() {
        return subCategoryDescription;
    }

    public void setSubCategoryDescription(String subCategoryDescription) {
        this.subCategoryDescription = subCategoryDescription;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", subCategoryId=" + subCategoryId + ", productServiceDescription=" + productServiceDescription + ", subCategoryDescription=" + subCategoryDescription + ", totalScore=" + totalScore + '}';
    }
}