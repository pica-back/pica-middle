package com.indicadores.bonita.repository;

import com.indicadores.bonita.repository.domain.VendorConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendorConfigurationRepository  extends JpaRepository<VendorConfiguration,Integer> {
    List<VendorConfiguration> findByState(
            String state
    );

    List<VendorConfiguration> findByName(
            String name
    );
}
