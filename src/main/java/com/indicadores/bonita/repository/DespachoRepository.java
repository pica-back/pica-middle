package com.indicadores.bonita.repository;

import com.indicadores.bonita.repository.domain.Despacho;
import com.indicadores.bonita.repository.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DespachoRepository extends JpaRepository<Despacho,Integer>{

    List<Despacho> findByIdclient(
            Integer idClient
    );

    List<Despacho> findByState(
            String state
    );
}
