package com.indicadores.bonita.repository;

import com.indicadores.bonita.repository.domain.Oferta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OfertaRepository extends JpaRepository<Oferta,Integer> {

    List<Oferta> findByIdproveedor(
            Integer idProveedor
    );

    List<Oferta> findByIddespachoOrderByValueAsc(
            Integer idDespacho
    );

}
