package com.indicadores.bonita.repository.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="vendor_configuration", catalog = "public")
public class VendorConfiguration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String endpoint;
    @Column
    private String method;
    @Column
    private String protocol;
    @Column
    private String state;
    @Column
    private String endpointms;
    @Column
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEndpointms() {
        return endpointms;
    }

    public void setEndpointms(String endpointms) {
        this.endpointms = endpointms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
