package com.indicadores.bonita.repository.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="oferta", catalog = "public")
public class Oferta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private Integer idccotizacion;
    @Column
    private Integer iddespacho;
    @Column
    private Integer idproveedor;
    @Column
    private Date date;
    @Column
    private Integer idservicio;
    @Column
    private float value;
    @Column
    private String state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdccotizacion() {
        return idccotizacion;
    }

    public void setIdccotizacion(Integer idccotizacion) {
        this.idccotizacion = idccotizacion;
    }

    public Integer getIddespacho() {
        return iddespacho;
    }

    public void setIddespacho(Integer iddespacho) {
        this.iddespacho = iddespacho;
    }

    public Integer getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(Integer idproveedor) {
        this.idproveedor = idproveedor;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Integer idservicio) {
        this.idservicio = idservicio;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
