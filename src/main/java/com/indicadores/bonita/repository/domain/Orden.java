package com.indicadores.bonita.repository.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="orden", catalog = "public")

public class Orden {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String email;
    @Column
    private String idorden;
    @Column
    private Date feccreacion;
    @Column
    private Date fecactualizacion;
    @Column
    private String direccion;
    @Column
    private Integer valor;
    @Column
    private Integer cantidad;
    @Column
    private String idproductprovider;
    @Column
    private String nomproducto;
    @Column
    private String proveedor;
    @Column
    private String proveedormnj;
    @Column
    private String estado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdorden() {
        return idorden;
    }

    public void setIdorden(String idorden) {
        this.idorden = idorden;
    }

    public Date getFeccreacion() {
        return feccreacion;
    }

    public void setFeccreacion(Date feccreacion) {
        this.feccreacion = feccreacion;
    }

    public Date getFecactualizacion() {
        return fecactualizacion;
    }

    public void setFecactualizacion(Date fecactualizacion) {
        this.fecactualizacion = fecactualizacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdproductprovider() {
        return idproductprovider;
    }

    public void setIdproductprovider(String idproductprovider) {
        this.idproductprovider = idproductprovider;
    }

    public String getNomproducto() {
        return nomproducto;
    }

    public void setNomproducto(String nomproducto) {
        this.nomproducto = nomproducto;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getProveedormnj() {
        return proveedormnj;
    }

    public void setProveedormnj(String proveedormnj) {
        this.proveedormnj = proveedormnj;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
