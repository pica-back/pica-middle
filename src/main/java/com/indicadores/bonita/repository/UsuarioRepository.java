package com.indicadores.bonita.repository;

import com.indicadores.bonita.repository.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Integer>{

    List<Usuario> findByEmail(
            String email
    );

}
