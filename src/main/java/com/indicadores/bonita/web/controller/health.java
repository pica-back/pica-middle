package com.indicadores.bonita.web.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
@RequestMapping("/health")
public class health {

    @GetMapping(value = "/", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> healthCheck() throws UnknownHostException {
        final String privateIp = InetAddress.getLocalHost().getHostAddress();
        final String privateDns = InetAddress.getLocalHost().getHostName();

        final String jsonResponse = "{" +
                "\"service\":\"backend-retrieve-products\"," +
                "\"status\":\"ok\"," +
                "\"privateIp\":\"" + privateIp + "\"," +
                "\"privateDns\":\"" + privateDns + "\"" +
                "}";

        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("dev-essentials-private-ip", privateIp);
        responseHeaders.set("dev-essentials-private-dns", privateDns);

        return ResponseEntity.ok().headers(responseHeaders).body(jsonResponse);
    }
}
