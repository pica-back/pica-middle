package com.indicadores.bonita.web.controller;

import com.indicadores.bonita.business.UsuarioBusiness;
import com.indicadores.bonita.dto.*;
import com.indicadores.bonita.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import java.net.UnknownHostException;
import java.net.InetAddress;
import java.util.List;

import org.springframework.http.HttpHeaders;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private DespachoRepository despachoRepository;

    @Autowired
    private OfertaRepository ofertaRepository;

    @Autowired
    private VendorConfigurationRepository vendorConfigurationRepository;

    @Autowired
    private OrdenRepository ordenRepository;

    @PostMapping("/registrar")
    public Respuesta registrarUsuario(@RequestBody UsuarioDto usuarioDto) {
        UsuarioBusiness usuarioBusiness = new UsuarioBusiness(usuarioRepository);
        return usuarioBusiness.registrarUsuarioBusiness(usuarioDto);
    }

    @GetMapping(value = "/consultar/despacho/{idClient}")
    public DespachoRtaDto consultarDespacho(@PathVariable("idClient") Integer idClient) {
        UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
        return usuarioBusiness.consultarDespachos(idClient, despachoRepository);
    }

    @PostMapping("/login")
    public UsuarioRtaDto loginUsuario(@RequestBody LoginDto loginDto) {
        UsuarioBusiness usuarioBusiness = new UsuarioBusiness(usuarioRepository);
        return usuarioBusiness.loginUsuario(loginDto);

    }

    @PostMapping("/crear/orden")
    public RespuestaOrdenDto crearOrden(@RequestBody RequestCreateOrderKsDto requestCreateOrderKsDto) {
        UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
        if (requestCreateOrderKsDto.getProveedor().equals("Kallsonys")){
            return usuarioBusiness.crearDespachoLocal(requestCreateOrderKsDto, ordenRepository, vendorConfigurationRepository);
        }else{
            return usuarioBusiness.crearDespachoProveedor(requestCreateOrderKsDto, ordenRepository, vendorConfigurationRepository);
        }
    }

    @PostMapping("/actualizar/oferta")
    public Respuesta actualizarOferta(@RequestBody ConfirmarDespachoDto confirmarDespachoDto) {
        UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
        return usuarioBusiness.actualizarOferta(confirmarDespachoDto, ofertaRepository, despachoRepository);

    }

    @GetMapping("/productos")
    public List<ProductoDto> listarProductos() {
        UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
        return usuarioBusiness.listarProducto(vendorConfigurationRepository);
    }

    @PostMapping("/enviar/notificacion")
    public Respuesta enviarNotificacion(@RequestBody EnviarNotificacionDto enviarNotificacionDto) {
        UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
        Respuesta respuesta = new Respuesta();
        respuesta.setStatusCode(0);
        respuesta.setMessage("prueba");
        usuarioBusiness.enviarNotificacion(despachoRepository,ofertaRepository,enviarNotificacionDto.getIdDespacho(),usuarioRepository);
        return respuesta;

    }

}
