package com.indicadores.bonita.mapper;

import com.indicadores.bonita.dto.CorreoRequestDto;
import com.indicadores.bonita.dto.RequestCreateOrderKsDto;
import com.indicadores.bonita.repository.domain.Orden;

public class EmailMapper {
    public CorreoRequestDto createRequestEmail(Orden orden){
        CorreoRequestDto correoRequestDto = new CorreoRequestDto();
        correoRequestDto.setSender("kallsonysnotifications@gmail.com");
        correoRequestDto.setReciever(orden.getEmail());
        correoRequestDto.setSubject("Orden "+ orden.getIdorden()+" creada Kallsonys");
        correoRequestDto.setContent("Se ha creado una orden con "+ orden.getCantidad() +
                " articulos, por un valor de "+ orden.getValor() + " y será despachada por "+orden.getProveedormnj());
        return correoRequestDto;
    }
}
