package com.indicadores.bonita.mapper;

import com.indicadores.bonita.dto.UsuarioDto;
import com.indicadores.bonita.repository.domain.Usuario;

public class UsuarioMapper {

    public Usuario fromUsuarioDtoToUsuario(UsuarioDto usuarioDto){
        Usuario usuario = new Usuario();
        //idRole: 1 - Cliente / 2 - Proveedor
        usuario.setIdrole(usuarioDto.getIdRole());
        usuario.setEmail(usuarioDto.getEmail());
        usuario.setIdtype(usuarioDto.getIdType());
        usuario.setNames(usuarioDto.getNames());
        usuario.setPassword(usuarioDto.getPassword());
        usuario.setSurmanes(usuarioDto.getSurmanes());
        usuario.setTelephone(usuarioDto.getTelephone());
        usuario.setId(usuarioDto.getId());
        return usuario;
    }

    public UsuarioDto fromUsuarioToUsuarioDto (Usuario usuario){
        UsuarioDto usuarioDto = new UsuarioDto();
        //idRole: 1 - Cliente / 2 - Proveedor
        usuarioDto.setIdRole(usuario.getIdrole());
        usuarioDto.setEmail(usuario.getEmail());
        //idType: 1 - NIT / 2 - CC
        usuarioDto.setIdType(usuario.getIdtype());
        usuarioDto.setNames(usuario.getNames());
        usuarioDto.setPassword(usuario.getPassword());
        usuarioDto.setSurmanes(usuario.getSurmanes());
        usuarioDto.setTelephone(usuario.getTelephone());
        usuarioDto.setId(usuario.getId());
        return usuarioDto;
    }
}
