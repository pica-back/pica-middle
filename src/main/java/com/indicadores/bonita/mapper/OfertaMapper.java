package com.indicadores.bonita.mapper;

import com.indicadores.bonita.dto.OfertaDto;
import com.indicadores.bonita.repository.domain.Oferta;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Integer.parseInt;

public class OfertaMapper {

    public OfertaDto fromOfertaToOfertaDto(Oferta oferta){
        OfertaDto ofertaDto = new OfertaDto();

        ofertaDto.setDate(oferta.getDate());
        ofertaDto.setId(oferta.getId());
        ofertaDto.setIdccotizacion(oferta.getIdccotizacion());
        ofertaDto.setIddespacho(oferta.getIddespacho());
        ofertaDto.setIdproveedor(oferta.getIdproveedor());
        ofertaDto.setIdservicio(oferta.getIdservicio());
        ofertaDto.setState(oferta.getState());
        ofertaDto.setValue(oferta.getValue());

        return ofertaDto;
    }

    public Oferta fromOfertaDtoToOferta(OfertaDto ofertaDto){
        Oferta oferta = new Oferta();

        oferta.setDate(ofertaDto.getDate());
        oferta.setId(ofertaDto.getId());
        oferta.setIdccotizacion(ofertaDto.getIdccotizacion());
        oferta.setIddespacho(ofertaDto.getIddespacho());
        oferta.setIdproveedor(ofertaDto.getIdproveedor());
        oferta.setIdservicio(ofertaDto.getIdservicio());
        oferta.setState(ofertaDto.getState());
        oferta.setValue(ofertaDto.getValue());

        return oferta;
    }

    public Oferta fronJsonObjectToOferta (JSONObject jSONObject,int idDespacho){
        Oferta oferta = new Oferta();
        oferta.setDate(new Date());
        oferta.setState("Ofertada");
        oferta.setIdccotizacion(parseInt(jSONObject.get("idccotizacion").toString()));
        oferta.setIdproveedor(parseInt(jSONObject.get("idproveedor").toString()));
        oferta.setIddespacho(idDespacho);
        oferta.setValue(parseInt(jSONObject.get("value").toString()));
        return oferta;
    }
}
