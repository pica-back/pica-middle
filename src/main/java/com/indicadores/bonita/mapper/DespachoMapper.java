package com.indicadores.bonita.mapper;

import com.indicadores.bonita.dto.DespachoDto;
import com.indicadores.bonita.repository.domain.Despacho;

public class DespachoMapper {

    public DespachoDto fromDespachoToDespachoDto(Despacho despacho){
        DespachoDto despachoDto = new DespachoDto();
        despachoDto.setAddress(despacho.getAddress());
        despachoDto.setDate(despacho.getDate());
        despachoDto.setDescription(despacho.getDescription());
        despachoDto.setEndcity(despacho.getEndcity());
        despachoDto.setId(despacho.getId());
        despachoDto.setIdclient(despacho.getIdclient());
        despachoDto.setName(despacho.getName());
        despachoDto.setStarcity(despacho.getStarcity());
        despachoDto.setState(despacho.getState());
        despachoDto.setValue(despacho.getValue());
        despachoDto.setVolume(despacho.getVolume());
        despachoDto.setWeight(despacho.getWeight());
        return despachoDto;
    }

    public Despacho fromDespachoDtoToDespacho(DespachoDto despachoDto){
        Despacho despacho = new Despacho();
        despacho.setAddress(despachoDto.getAddress());
        despacho.setDate(despachoDto.getDate());
        despacho.setDescription(despachoDto.getDescription());
        despacho.setEndcity(despachoDto.getEndcity());
        despacho.setId(despachoDto.getId());
        despacho.setIdclient(despachoDto.getIdclient());
        despacho.setName(despachoDto.getName());
        despacho.setStarcity(despachoDto.getStarcity());
        despacho.setState(despachoDto.getState());
        despacho.setValue(despachoDto.getValue());
        despacho.setVolume(despachoDto.getVolume());
        despacho.setWeight(despachoDto.getWeight());
        return despacho;
    }
}
