package com.indicadores.bonita.mapper;

import com.indicadores.bonita.dto.RequestCreateOrderKsDto;
import com.indicadores.bonita.repository.domain.Orden;
import java.util.Date;

public class OrdenMapper {

    public Orden fronDtoToTbl(RequestCreateOrderKsDto requestCreateOrderKsDto, String proveedorMsj){
        Orden orden = new Orden();
        orden.setEmail(requestCreateOrderKsDto.getEmail());
        orden.setIdorden(String.valueOf((int)(Math. random()*1000+1)));
        orden.setFeccreacion(new Date());
        orden.setFecactualizacion(new Date());
        orden.setDireccion(requestCreateOrderKsDto.getDireccion());
        orden.setValor(requestCreateOrderKsDto.getValor());
        orden.setCantidad(requestCreateOrderKsDto.getCantidad());
        orden.setIdproductprovider(requestCreateOrderKsDto.getIdproductprovider());
        orden.setNomproducto(requestCreateOrderKsDto.getNomproducto());
        orden.setProveedor(requestCreateOrderKsDto.getProveedor());
        orden.setProveedormnj(proveedorMsj);
        orden.setEstado(requestCreateOrderKsDto.getEstado());
        return orden;
    }

    public Orden fronDtoToTblOrdenProveedor(RequestCreateOrderKsDto requestCreateOrderKsDto, String idOrden){
        Orden orden = new Orden();
        orden.setEmail(requestCreateOrderKsDto.getEmail());
        orden.setIdorden(idOrden);
        orden.setFeccreacion(new Date());
        orden.setFecactualizacion(new Date());
        orden.setDireccion(requestCreateOrderKsDto.getDireccion());
        orden.setValor(requestCreateOrderKsDto.getValor());
        orden.setCantidad(requestCreateOrderKsDto.getCantidad());
        orden.setIdproductprovider(requestCreateOrderKsDto.getIdproductprovider());
        orden.setNomproducto(requestCreateOrderKsDto.getNomproducto());
        orden.setProveedor(requestCreateOrderKsDto.getProveedor());
        orden.setProveedormnj(requestCreateOrderKsDto.getProveedor());
        orden.setEstado(requestCreateOrderKsDto.getEstado());
        return orden;
    }
}
