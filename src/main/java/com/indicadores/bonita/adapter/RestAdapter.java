package com.indicadores.bonita.adapter;

import com.indicadores.bonita.dto.*;
import com.indicadores.bonita.repository.domain.Oferta;
import org.springframework.jdbc.JdbcUpdateAffectedIncorrectNumberOfRowsException;
import org.springframework.web.client.RestTemplate;
import org.json.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class RestAdapter {
    //String uri ="https://a43852b2-3dcd-4883-b442-a43432b03175.mock.pstmn.io/prueba";
    RestTemplate restTemplate;

    public RestAdapter() {
        restTemplate = new RestTemplate();
    }

    public CompletableFuture<Respuesta> cotizarRest(RequestCreateOrderKsDto requestCreateOrderKsDto, String uri) {

        final Respuesta responseBody = restTemplate.postForObject(uri, requestCreateOrderKsDto, Respuesta.class);
        return CompletableFuture.completedFuture(responseBody);

    }

    public CompletableFuture<Object> adpRestPOST(CorreoRequestDto body, String uri) {

        final Object responseBody = restTemplate.postForObject(uri, body, Object.class);
        return CompletableFuture.completedFuture(responseBody);
    }

    public CompletableFuture<RespuestaProductos> adptRestGET(String uri) {

        System.out.println("uri" + uri);
        final RespuestaProductos responseBody = restTemplate.getForObject(uri, RespuestaProductos.class);
        System.out.println(responseBody.toString());
        return CompletableFuture.completedFuture(responseBody);
    }

    public CompletableFuture<VendorMsj> adpRestGET(String uri) {

        final VendorMsj responseBody = restTemplate.getForObject(uri, VendorMsj.class);

        return CompletableFuture.completedFuture(responseBody);
    }

}
